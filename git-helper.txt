Le 19-02-2019 - Git
===================================================================================================

git add fichier : Rajouter ce qui a chang� dans un fichier � la prochaine version
		  Place les fichiers dans la 'staging area' (zone de recette)

git commit -m "Comment" : Cr�e une version en envoyant les fichiers
git commit -am "Comment" : Ajoute tous les fichiers que GIT connait d�j�
git commit -am "New comment" --amend : Permet de modifier le pr�c�dent commit (commentaire et/ou fichiers)

git log : Affichage de l'historique du repository
git log -1 : N'affiche qu'un seul r�sultat
git log --stat : Affiche en quel mesure les fichiers ont �t� modifi�s pendant le commit
git log --oneline : Affiche les logs sur une seule ligne
git log -p : Affiche le d�tails des modifications � l'�cran
git log --since "last week" : Afficher les logs depuis la semaine derni�re
git log --since "10 minutes" : Afficher les logs depuis 10 minutes

git status : Montre l'�tat permanent du d�pot contre la copie locale

git checkout -- test.txt : Permet de r�cup�rer le fichier qui se trouve sur le d�p�t vers le local
git checkout <num_version> -- test.txt : Permet de restaurer un fichier � la version 'n'
git checkout HEAD~ -- test.txt : R�cup�re le fichier � une version n-1
				 Le nombre de ~ incr�mente la version du nombre de version
				 On peut afficher aussi ~3 pour le nombre de version ant�rieure (ici 3)

git checkout -b <branch_name> : Cr�e une branche � part de ma copie principale

git reset <num_version> : Revient au commit pr�c�dent en ajoutant, supprimant, modifiant les fichiers et leurs contenus.
			  ATTENTION : Les commit annul�s sont perdus !

git diff : Permet de voir la diff�rence entre le local et le 'Staging area'

git merge <branch_name> : Depuis le master, vient fusioner le contenu des branches vers le local


Le 20-02-2019 - Git
===================================================================================================

D�p�t distant : clone / push
Cr�ation et utilistation d'une clef SSH avec putty.
Configuration de la clef sur gitlab, de fa�on � ne pas avoir � se connecter � chaque fois.
export GIT_SSH='plink' : Pour activer un plink en SSH sur gitlab

Cr�ation du projet sur git lab, et push par les commandes suivantes : 
git remote add origin git@gitlab.coom:kamui.studioo/git-helper-project.git

git fetch : Permet de mettre � jour

Maintenant, pour avoir des �l�ments identiques sur deux r�pertoires diff�rents

Le 20-02-2019 - Jenkins
===================================================================================================

On va cr�er un artefact � jour, sur un build avec Jenkins